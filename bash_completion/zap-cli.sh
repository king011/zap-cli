#!/bin/bash
#Program:
#       zap-cli for bash completion
#History:
#       2018-10-13 king011 first release
#       2018-10-16 king011 fix style
#Email:
#       zuiwuchang@gmail.com

function __king011_zap_cli_watch()
{
    local opts='-f --filename -a --auto -u --url -h --help'
    case ${COMP_WORDS[COMP_CWORD-1]} in
        -f|--filename)
            _filedir || COMPREPLY=( $(compgen -o plusdirs -f ${cur}) )
        ;;
        -u|--url)
            COMPREPLY=()
        ;;

        # default
        *)
            COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
        ;;
    esac
}
function __king011_zap_cli_analyze()
{
    # 參數定義
    local opts='-f --filename -h --help'
    case ${COMP_WORDS[COMP_CWORD-1]} in
        -f|--filename)
           _filedir || COMPREPLY=( $(compgen -o plusdirs -f ${cur}) )
        ;;

        # default
        *)
            COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
        ;;
    esac
}
function __king011_zap_cli()
{
    # 獲取 正在輸入的 參數
    local cur=${COMP_WORDS[COMP_CWORD]}
    
    #  輸入 第1個 參數
    if [ 1 == $COMP_CWORD ];then
        local opts="watch analyze help --help -h -v --version"
        COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
    else
        # switch 子命令
        case ${COMP_WORDS[1]} in
            watch)
                __king011_zap_cli_watch
            ;;

            analyze)
               __king011_zap_cli_analyze
            ;;
        esac
    fi
}

complete -F __king011_zap_cli zap-cli