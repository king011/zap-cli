package analyze

// Run .
func Run(files []string) (e error) {
	var a *Analyze
	a, e = New(files)
	if e != nil {
		return
	}
	a.Run()
	return
}
