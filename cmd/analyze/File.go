package analyze

import (
	kString "gitlab.com/king011/king-go/strings"
	"gitlab.com/king011/zap-cli/utils"
	"io/ioutil"
	"log"
	"path"
	"strings"
	"time"
)

// File 日誌 檔案
type File struct {
	Name  string
	Begin time.Time
	End   time.Time
	Items []*utils.Item
}

// NewFile 從 日誌檔案 創建 內存結構
func NewFile(filename string) (f *File, e error) {
	var b []byte
	b, e = ioutil.ReadFile(filename)
	if e != nil {
		return
	}
	str := strings.TrimSpace(kString.BytesToString(b))
	strs := strings.Split(str, "\n")
	items := make([]*utils.Item, 0, len(strs))
	var item *utils.Item
	for i := 0; i < len(strs); i++ {
		item, e = utils.NewItem(kString.StringToBytes(strs[i]))
		if e != nil {
			log.Panicln(e)
			continue
		}
		items = append(items, item)
	}
	if len(items) == 0 {
		return
	}

	f = &File{
		Name:  path.Base(filename),
		Begin: items[0].Time,
		End:   items[len(items)-1].Time,
		Items: items,
	}
	return
}

type fileSort []*File

func (a fileSort) Len() int {
	return len(a)
}
func (a fileSort) Less(i, j int) bool {
	return a[i].Begin.Before(a[j].Begin)
}
func (a fileSort) Swap(i, j int) {
	a[i], a[j] = a[j], a[i]
}
