package analyze

import (
	"bufio"
	"fmt"
	kString "gitlab.com/king011/king-go/strings"
	"gitlab.com/king011/zap-cli/utils"
	"log"
	"os"
	"regexp"
	"sort"
	"strings"
)

// Analyze .
type Analyze struct {
	fs        []*File
	num       int64
	findRules map[string]utils.Rule
}

// New .
func New(filenames []string) (a *Analyze, e error) {
	var f *File
	fs := make([]*File, 0, 10)
	var num int64
	for i := 0; i < len(filenames); i++ {
		f, e = NewFile(filenames[i])
		if e != nil {
			return
		} else if f != nil {
			fs = append(fs, f)
			num += int64(len(f.Items))
		}
	}
	sort.Sort(fileSort(fs))

	a = &Analyze{
		fs:        fs,
		num:       num,
		findRules: make(map[string]utils.Rule),
	}
	return
}
func (a *Analyze) showHelp() {
	fmt.Print(`e/q/exit/quit : exit process
h/help : print help info
info : display watcher info
f/find-[key]-[regexp] : if key match find [level caller ts msg]
f/find-[key] : clear key find rule
g/get : get all that match
`)
}
func (a *Analyze) setFind(key, val string) {
	reg, e := regexp.Compile(val)
	if e != nil {
		log.Println(e)
		return
	}

	a.findRules[key] = utils.Rule{
		Regexp: reg,
		Val:    val,
	}
}
func (a *Analyze) eraseFind(key string) {
	if _, ok := a.findRules[key]; ok {
		delete(a.findRules, key)
	}
}
func (a *Analyze) get() {
	var file *File
	for i := 0; i < len(a.fs); i++ {
		file = a.fs[i]
		for j := 0; j < len(file.Items); j++ {
			a.output(a.findRules, file.Items[j])
		}
	}
}
func (a *Analyze) output(rules map[string]utils.Rule, item *utils.Item) {
	if len(rules) == 0 {
		item.Output()
		return
	}
	for k, v := range rules {
		if !item.Match(k, v.Regexp) {
			return
		}
	}
	item.Output()
}
func (a *Analyze) info() {
	fmt.Println("logs : [")
	for i := 0; i < len(a.fs); i++ {
		fmt.Printf("   [%v] [%v] [%v - %v]\n",
			a.fs[i].Name, len(a.fs[i].Items),
			a.fs[i].Begin, a.fs[i].End,
		)
	}
	fmt.Println("]")
	fmt.Println("items :", a.num)
	fmt.Print("find : [ ")
	if len(a.findRules) != 0 {
		i := 0
		for key, rule := range a.findRules {
			if i == 0 {
				fmt.Print(key + ":" + rule.Val)
				i++
			} else {
				fmt.Print(" , ", key+":"+rule.Val)
			}
		}
	}
	fmt.Println(" ]")
}

// Run .
func (a *Analyze) Run() {
	var str string
	var b []byte
	var e error
	for {
		fmt.Print("$>")
		r := bufio.NewReader(os.Stdin)
		b, _, e = r.ReadLine()
		if e != nil {
			log.Println(e)
			continue
		}
		str = strings.TrimSpace(kString.BytesToString(b))
		if str == "" {
			continue
		}
		if str == "e" || str == "q" || str == "exit" || str == "quit" {
			break
		} else if str == "h" || str == "help" {
			a.showHelp()
		} else if strings.HasPrefix(str, "find-") {
			str = str[len("find-"):]
			strs := strings.SplitN(str, "-", 2)
			if len(strs) == 2 {
				a.setFind(strs[0], strs[1])
			} else {
				a.eraseFind(strs[0])
			}
		} else if strings.HasPrefix(str, "f-") {
			str = str[len("f-"):]
			strs := strings.SplitN(str, "-", 2)
			if len(strs) == 2 {
				a.setFind(strs[0], strs[1])
			} else {
				a.eraseFind(strs[0])
			}
		} else if str == "info" {
			a.info()
		} else if str == "g" || str == "get" {
			a.get()
		} else {
			a.showHelp()

			fmt.Println("\nunknow command : " + str)
		}
	}
}
