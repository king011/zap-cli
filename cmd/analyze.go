package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/king011/zap-cli/cmd/analyze"
	"log"
)

func init() {
	var files []string
	cmd := &cobra.Command{
		Use:   "analyze",
		Short: "analyze zap logs",
		Long: `analyze zap logs
	zap-cli analyze -f logs/app.log -f logs/app-1.log -f logs/app-x.log
`,
		Run: func(cmd *cobra.Command, args []string) {
			if len(files) == 0 {
				cmd.Help()
				return
			}
			e := analyze.Run(files)
			if e != nil {
				log.Fatalln(e)
			}
		},
	}
	flags := cmd.Flags()
	flags.StringSliceVarP(&files,
		"filename", "f",
		nil,
		"log files",
	)

	rootCmd.AddCommand(cmd)
}
