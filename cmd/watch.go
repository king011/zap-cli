package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/king011/zap-cli/cmd/watch"
	"log"
	"path/filepath"
)

func init() {
	var context watch.Context
	var filename string
	var auto bool
	cmd := &cobra.Command{
		Use:   "watch",
		Short: "watch zap logs changed",
		Long: `watch zap log changed
	zap-cli watch
`,
		Run: func(cmd *cobra.Command, args []string) {
			var e error
			if !filepath.IsAbs(filename) {
				filename, e = filepath.Abs(filename)
				if e != nil {
					log.Fatalln(e)
				}
			}
			context.Directory = filepath.Dir(filename)
			context.Filename = filepath.Base(filename)

			e = watch.Run(context, auto)
			if e != nil {
				log.Fatalln(e)
			}
		},
	}

	flags := cmd.Flags()
	flags.StringVarP(&filename,
		"filename", "f",
		"logs/app.log",
		"log's filename",
	)
	flags.StringVarP(&context.URL,
		"url", "u",
		"http://localhost:20000",
		"zap http request address",
	)
	flags.BoolVarP(&auto,
		"auto", "a",
		false,
		"if ture auto run monitor",
	)

	rootCmd.AddCommand(cmd)
}
