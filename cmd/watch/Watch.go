package watch

import (
	"bufio"
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/fsnotify/fsnotify"
	kString "gitlab.com/king011/king-go/strings"
	"gitlab.com/king011/zap-cli/utils"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"regexp"
	"runtime"
	"strings"
	"sync"
	"time"
)

// Watcher 日誌 監控器
type Watcher struct {
	mutex   sync.Mutex
	context Context
	watcher *fsnotify.Watcher
	wg      sync.WaitGroup

	b []byte
	// 日誌 記錄
	items []string
	cache map[string]*utils.Item

	// 是否 顯示到 控制檯
	show bool

	isMonitor bool

	showRules map[string]utils.Rule
	findRules map[string]utils.Rule

	signal chan interface{}
}

// New 創建一個 日誌 監控器
func New(context Context, auto bool) (watcher *Watcher, e error) {
	if runtime.GOOS == "windows" {
		watcher = &Watcher{
			context:   context,
			show:      true,
			isMonitor: auto,
			showRules: make(map[string]utils.Rule),
			findRules: make(map[string]utils.Rule),
			signal:    make(chan interface{}),
		}
		watcher.wg.Add(1)
		log.Println("run monitor goroutine")
		go watcher.runMonitorWindows(context.Directory + "/" + context.Filename)
	} else {
		// 創建 監控器
		var fwatcher *fsnotify.Watcher
		fwatcher, e = fsnotify.NewWatcher()
		if e != nil {
			return
		}
		// 設置要監視的 目錄
		if auto {
			e = fwatcher.Add(context.Directory)
			if e != nil {
				return
			}
		}

		watcher = &Watcher{
			context:   context,
			watcher:   fwatcher,
			show:      true,
			isMonitor: auto,
			showRules: make(map[string]utils.Rule),
			findRules: make(map[string]utils.Rule),
		}
		watcher.wg.Add(1)
		log.Println("run monitor goroutine")
		go watcher.runMonitor(context.Filename)
	}
	return
}
func (w *Watcher) runMonitorWindows(filename string) {
	defer func() {
		log.Println("exit monitor goroutine")
		w.wg.Done()
	}()
	duration := time.Millisecond * 100
	t := time.NewTimer(duration)
	var e error
	var n, last int64
	var monitor bool
	for {
		select {
		case <-w.signal:
			if !t.Stop() {
				<-t.C
			}
			return
		case <-t.C:
			w.mutex.Lock()
			monitor = w.isMonitor
			w.mutex.Unlock()
			if monitor {
				n, e = w.windowsCheckLog(filename)
				if e == nil && n != last {
					last = n
					w.onLogChange(filename)
				}
			}
			t.Reset(duration)
		}
	}
}
func (w *Watcher) windowsCheckLog(filename string) (n int64, e error) {
	var info os.FileInfo
	info, e = os.Stat(filename)
	if e != nil {
		log.Println(e)
		return
	} else if info.IsDir() {
		e = fmt.Errorf("[%v] is not a log file", filename)
		return
	}
	n = info.Size()
	return
}
func (w *Watcher) runMonitor(logname string) {
	defer func() {
		log.Println("exit monitor goroutine")
		w.wg.Done()
	}()
	watcher := w.watcher
	var e error
	var ok bool
	var evt fsnotify.Event
	var fileName, base string
	var fileInfo os.FileInfo
	for {
		select {
		case evt, ok = <-watcher.Events:
			if !ok {
				return
			}
			if evt.Op&fsnotify.Write == fsnotify.Write {
				fileName = evt.Name
				base = filepath.Base(fileName)
				fileInfo, e = os.Stat(fileName)
				if e == nil {
					if !fileInfo.IsDir() &&
						base == logname {
						w.onLogChange(fileName)
					}
				} else {
					log.Println(e)
				}
			}
		case e, ok = <-watcher.Errors:
			if e != nil {
				log.Println(e)
			}
			if !ok {
				return
			}
		}
	}
}
func (w *Watcher) readFile(filename string) (cache []byte, str string, e error) {
	w.mutex.Lock()
	b := w.b
	w.mutex.Unlock()
	if len(b) == 0 {
		b, e = ioutil.ReadFile(filename)
		if e != nil {
			log.Println(e)
			return
		}
		w.mutex.Lock()
		for k := range w.cache {
			delete(w.cache, k)
		}
		w.mutex.Unlock()
	} else {
		var f *os.File
		f, e = os.Open(filename)
		if e != nil {
			return
		}
		defer f.Close()
		var fi os.FileInfo
		fi, e = f.Stat()
		if e != nil {
			return
		}
		if fi.Size() < int64(len(b)) {
			// 新日誌檔案
			b, e = ioutil.ReadAll(f)
			if e != nil {
				return
			}
			w.mutex.Lock()
			for k := range w.cache {
				delete(w.cache, k)
			}
			w.mutex.Unlock()
		} else {
			// 舊日誌檔案
			_, e = f.Seek(int64(len(b)), os.SEEK_SET)
			if e != nil {
				return
			}
			var br []byte
			br, e = ioutil.ReadAll(f)
			if e != nil {
				return
			}
			b = append(b, br...)
		}

	}
	str = kString.BytesToString(b)
	return
}
func (w *Watcher) onLogChange(filename string) {
	cache, str, e := w.readFile(filename)
	if e != nil {
		return
	}
	str = strings.TrimSpace(str)
	items := strings.Split(str, "\n")

	w.mutex.Lock()
	defer w.mutex.Unlock()

	pos := len(w.items)
	w.items = items
	w.b = cache
	if w.show {
		for i := pos; i < len(w.items); i++ {
			item := w.findItem(w.items[i])
			if item != nil {
				w.output(w.showRules, item)
			}
		}
	}
}
func (w *Watcher) output(rules map[string]utils.Rule, item *utils.Item) {
	if len(rules) == 0 {
		item.Output()
		return
	}
	for k, v := range rules {
		if !item.Match(k, v.Regexp) {
			return
		}
	}
	item.Output()
}
func (w *Watcher) findItem(str string) (item *utils.Item) {
	var ok bool
	item, ok = w.cache[str]
	if ok {
		return
	}
	var e error
	item, e = utils.NewItem(kString.StringToBytes(str))
	if e != nil {
		log.Println(e)
	}
	return
}
func (w *Watcher) run() {
	w.mutex.Lock()
	if w.isMonitor {
		w.mutex.Unlock()
		return
	}
	w.isMonitor = true
	if w.watcher != nil {
		w.watcher.Add(w.context.Directory)
	}
	w.mutex.Unlock()
}
func (w *Watcher) stop() {
	w.mutex.Lock()
	if !w.isMonitor {
		w.mutex.Unlock()
		return
	}
	w.isMonitor = false
	if w.watcher != nil {
		w.watcher.Remove(w.context.Directory)
	}
	w.mutex.Unlock()
}
func (w *Watcher) clear() {
	w.mutex.Lock()
	w.b = nil
	w.items = nil
	for k := range w.cache {
		delete(w.cache, k)
	}
	w.mutex.Unlock()
}
func (w *Watcher) get() {
	num := 0
	w.mutex.Lock()
	if len(w.findRules) == 0 {
		for i := 0; i < len(w.items); i++ {
			item := w.findItem(w.items[i])
			if item != nil {
				item.Output()
				num++
			}
		}
	} else {
		for i := 0; i < len(w.items); i++ {
			item := w.findItem(w.items[i])
			if item != nil {
				w.output(w.findRules, item)
				num++
			}
		}
	}
	w.mutex.Unlock()
	fmt.Println("items :", num)
}
func (w *Watcher) info() {
	w.mutex.Lock()

	fmt.Println("directory :", w.context.Directory)
	fmt.Println("filename :", w.context.Filename)
	fmt.Println("http :", w.context.URL)
	fmt.Println("items :", len(w.items))
	fmt.Println("show :", w.show)
	fmt.Println("monitor :", w.isMonitor)
	fmt.Print("rules : [ ")
	if len(w.showRules) != 0 {
		i := 0
		for key, rule := range w.showRules {
			if i == 0 {
				fmt.Print(key + ":" + rule.Val)
				i++
			} else {
				fmt.Print(" , ", key+":"+rule.Val)
			}
		}
	}
	fmt.Println(" ]")
	fmt.Print("find : [ ")
	if len(w.findRules) != 0 {
		i := 0
		for key, rule := range w.findRules {
			if i == 0 {
				fmt.Print(key + ":" + rule.Val)
				i++
			} else {
				fmt.Print(" , ", key+":"+rule.Val)
			}
		}
	}
	fmt.Println(" ]")
	w.mutex.Unlock()
}
func (w *Watcher) setRule(key, val string) {
	reg, e := regexp.Compile(val)
	if e != nil {
		log.Println(e)
		return
	}
	w.mutex.Lock()
	w.showRules[key] = utils.Rule{
		Regexp: reg,
		Val:    val,
	}
	w.mutex.Unlock()
}
func (w *Watcher) setFind(key, val string) {
	reg, e := regexp.Compile(val)
	if e != nil {
		log.Println(e)
		return
	}
	w.mutex.Lock()
	w.findRules[key] = utils.Rule{
		Regexp: reg,
		Val:    val,
	}
	w.mutex.Unlock()
}
func (w *Watcher) eraseRule(key string) {
	w.mutex.Lock()
	if _, ok := w.showRules[key]; ok {
		delete(w.showRules, key)
	}
	w.mutex.Unlock()
}
func (w *Watcher) eraseFind(key string) {
	w.mutex.Lock()
	if _, ok := w.findRules[key]; ok {
		delete(w.findRules, key)
	}
	w.mutex.Unlock()
}
func (w *Watcher) showHelp() {
	fmt.Print(`e/q/exit/quit : exit process
h/help : print help info
run : run log watcher
stop : stop log watcher
clear : clear log cache
show : display log to console
hide : hide console log display
info : display watcher info
r/rule-[key]-[regexp] : if key match display [level caller ts msg]
r/rule-[key] : clear key display rule
f/find-[key]-[regexp] : if key match find [level caller ts msg]
f/find-[key] : clear key find rule
g/get : get all that match
set-[level] : send http request set zap level [debug info warn error dpanic panic fatal]
set : send http request get zap level
`)
}
func (w *Watcher) httpGet() {
	w.mutex.Lock()
	url := w.context.URL
	w.mutex.Unlock()

	r, e := http.Get(url)
	if e != nil {
		log.Println(e)
		return
	}
	b, e := ioutil.ReadAll(r.Body)
	if e != nil {
		log.Println(e)
		return
	}
	fmt.Println(kString.BytesToString(b))
}
func (w *Watcher) httpPut(str string) {
	str = strings.ToLower(strings.TrimSpace(str))
	w.mutex.Lock()
	url := w.context.URL
	w.mutex.Unlock()

	b, e := json.Marshal(&struct {
		Level string
	}{
		Level: str,
	})
	if e != nil {
		log.Panicln(e)
		return
	}
	body := bytes.NewBuffer(b)
	client := &http.Client{}
	var request *http.Request
	request, e = http.NewRequest("PUT", url, body)
	if e != nil {
		log.Println(e)
		return
	}
	var response *http.Response
	response, e = client.Do(request)
	if e != nil {
		log.Println(e)
		return
	}
	b, e = ioutil.ReadAll(response.Body)
	if e != nil {
		log.Println(e)
		return
	}
	fmt.Println(kString.BytesToString(b))
}

// Run 運行 命令行
func (w *Watcher) Run() {
	var str string
	var b []byte
	var e error
	for {
		fmt.Print("$>")
		r := bufio.NewReader(os.Stdin)
		b, _, e = r.ReadLine()
		if e != nil {
			log.Println(e)
			continue
		}
		str = strings.TrimSpace(kString.BytesToString(b))
		if str == "" {
			continue
		}
		if str == "e" || str == "q" || str == "exit" || str == "quit" {
			break
		} else if str == "h" || str == "help" {
			w.showHelp()
		} else if str == "show" {
			w.mutex.Lock()
			w.show = true
			w.mutex.Unlock()
		} else if str == "hide" {
			w.mutex.Lock()
			w.show = false
			w.mutex.Unlock()
		} else if str == "run" {
			w.run()
		} else if str == "stop" {
			w.stop()
		} else if str == "clear" {
			w.clear()
		} else if str == "info" {
			w.info()
		} else if str == "g" || str == "get" {
			w.get()
		} else if str == "set" {
			w.httpGet()
		} else if strings.HasPrefix(str, "set-") {
			str = str[len("set-"):]
			w.httpPut(str)
		} else if strings.HasPrefix(str, "rule-") {
			str = str[len("rule-"):]
			strs := strings.SplitN(str, "-", 2)
			if len(strs) == 2 {
				w.setRule(strs[0], strs[1])
			} else {
				w.eraseRule(strs[0])
			}
		} else if strings.HasPrefix(str, "r-") {
			str = str[len("r-"):]
			strs := strings.SplitN(str, "-", 2)
			if len(strs) == 2 {
				w.setRule(strs[0], strs[1])
			} else {
				w.eraseRule(strs[0])
			}
		} else if strings.HasPrefix(str, "find-") {
			str = str[len("find-"):]
			strs := strings.SplitN(str, "-", 2)
			if len(strs) == 2 {
				w.setFind(strs[0], strs[1])
			} else {
				w.eraseFind(strs[0])
			}
		} else if strings.HasPrefix(str, "f-") {
			str = str[len("f-"):]
			strs := strings.SplitN(str, "-", 2)
			if len(strs) == 2 {
				w.setFind(strs[0], strs[1])
			} else {
				w.eraseFind(strs[0])
			}
		} else {
			w.showHelp()

			fmt.Println("\nunknow command : " + str)
		}
	}
	if w.watcher == nil {
		close(w.signal)
	} else {
		w.watcher.Close()
	}
	w.wg.Wait()
	log.Println("exit process")
}
