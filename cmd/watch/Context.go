package watch

// Context .
type Context struct {
	// 日誌 檔案名
	Filename string
	// 日誌所在檔案夾
	Directory string
	// zap 日誌等級 http 地址
	URL string
}
