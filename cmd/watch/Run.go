package watch

// Run 運行 watch
func Run(ctx Context, auto bool) (e error) {
	var watcher *Watcher
	watcher, e = New(ctx, auto)
	if e != nil {
		return
	}

	watcher.Run()
	return
}
