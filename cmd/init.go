package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/king011/zap-cli/version"
	"os"
)

const (
	// App 程式名
	App = "zap-cli"
)

var v bool
var rootCmd = &cobra.Command{
	Use:   App,
	Short: "zap json log analyzing tool",
	Run: func(cmd *cobra.Command, args []string) {
		if v {
			fmt.Println(version.Tag)
			fmt.Println(version.Commit)
			fmt.Println(version.Date)
		} else {
			fmt.Println(App)
			fmt.Println(version.Tag)
			fmt.Println(version.Commit)
			fmt.Println(version.Date)
			fmt.Printf(`Use "%v --help" for more information about this program.
`, App)
		}
	},
}

func init() {
	flags := rootCmd.Flags()
	flags.BoolVarP(&v,
		"version",
		"v",
		false,
		"show version",
	)
}

// Execute 執行命令
func Execute() error {
	return rootCmd.Execute()
}
func abort() {
	os.Exit(1)
}
