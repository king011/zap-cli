package utils

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/fatih/color"
	kString "gitlab.com/king011/king-go/strings"
	"go.uber.org/zap/zapcore"
	"os"
	"regexp"
	"strings"
	"time"
)

var _colors = make(map[zapcore.Level]*color.Color)

func init() {
	_colors[zapcore.DebugLevel] = color.New(color.FgCyan)
	_colors[zapcore.InfoLevel] = color.New(color.FgGreen)
	_colors[zapcore.WarnLevel] = color.New(color.FgYellow)
	_colors[zapcore.ErrorLevel] = color.New(color.FgMagenta)

	_colors[zapcore.DPanicLevel] = color.New(color.FgRed)
	_colors[zapcore.PanicLevel] = color.New(color.FgRed)
	_colors[zapcore.FatalLevel] = color.New(color.FgRed)
}

// Item 一條 日記 記錄
type Item struct {
	// 日誌 等級
	Level zapcore.Level
	// 日誌 時間
	Time time.Time
	// 調用 位置
	Caller string

	Message string

	// 額外屬性
	Keys map[string]interface{}
}

// NewItem .
func NewItem(b []byte) (item *Item, e error) {
	var keys map[string]interface{}
	e = json.Unmarshal(b, &keys)
	if e != nil {
		return
	}
	var tmp Item
	if v, ok := keys["level"]; ok {
		if level, ok := v.(string); ok {
			switch level {
			case "debug":
				tmp.Level = zapcore.DebugLevel
			case "info":
				tmp.Level = zapcore.InfoLevel
			case "warn":
				tmp.Level = zapcore.WarnLevel
			case "error":
				tmp.Level = zapcore.ErrorLevel

			case "dpanic":
				tmp.Level = zapcore.DPanicLevel
			case "panic":
				tmp.Level = zapcore.PanicLevel
			case "fatal":
				tmp.Level = zapcore.FatalLevel
			default:
				e = fmt.Errorf("unknow level %s", b)
			}
		} else {
			e = fmt.Errorf("unknow level %s", b)
			return
		}
	} else {
		e = fmt.Errorf("unknow level %s", b)
		return
	}
	delete(keys, "level")
	if v, ok := keys["ts"]; ok {
		if ts, ok := v.(float64); ok {
			secs := int64(ts)
			nsecs := int64((ts - float64(secs)) * 1e9)
			tmp.Time = time.Unix(secs, nsecs)
		}
		delete(keys, "ts")
	}
	if v, ok := keys["caller"]; ok {
		if caller, ok := v.(string); ok {
			tmp.Caller = caller
		}
		delete(keys, "caller")
	}
	if v, ok := keys["msg"]; ok {
		if msg, ok := v.(string); ok {
			tmp.Message = msg
		}
		delete(keys, "msg")
	}
	item = &tmp
	tmp.Keys = keys
	return
}

// Output 打印到 stdcerr
func (i *Item) Output() {
	color := _colors[i.Level]
	if color == nil {
		return
	}
	color.Fprintln(os.Stderr, i)
}
func (i *Item) String() string {
	var buf bytes.Buffer
	buf.WriteString(fmt.Sprintln(i.Level, i.Caller, i.Time))

	strs := strings.Split(i.Message, "\n")
	for _, str := range strs {
		buf.WriteString("--  ")
		buf.WriteString(str)
		buf.WriteString("\n")
	}

	for k, v := range i.Keys {
		var strs []string
		if ts, ok := v.(float64); ok {
			secs := int64(ts)
			nsecs := int64((ts - float64(secs)) * 1e9)
			t := time.Unix(secs, nsecs)
			strs = make([]string, 1)
			strs[0] = fmt.Sprintf("%v [%v]", v, t)
		} else {
			strs = strings.Split(fmt.Sprint(v), "\n")
		}
		for i, str := range strs {
			buf.WriteString("**  ")
			buf.WriteString(k)
			if i == 0 {
				b := make([]byte, len(k))
				for j := 0; j < len(b); j++ {
					b[j] = ' '
				}
				k = kString.BytesToString(b)
			}
			buf.WriteString(" = ")
			buf.WriteString(str)
			buf.WriteString("\n")
		}
	}
	return buf.String()
}

// Match .
func (i *Item) Match(key string, reg *regexp.Regexp) (yes bool) {
	var val string
	switch key {
	case "level":
		val = i.Level.String()
	case "caller":
		val = i.Caller
	case "ts":
		val = i.Time.String()
	case "msg":
		val = i.Message
	default:
		var findOk bool
		find, findOk := i.Keys[key]
		if !findOk {
			return
		}
		val = fmt.Sprint(find)
	}
	yes = reg.MatchString(fmt.Sprint(val))
	return
}
