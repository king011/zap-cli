package utils

import (
	"regexp"
)

// Rule .
type Rule struct {
	Val    string
	Regexp *regexp.Regexp
}
