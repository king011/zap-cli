# zap-cli

zap log analyzing tool


zap-cli 是一個 [https://github.com/uber-go/zap](https://github.com/uber-go/zap) 的 json 日誌檔案 分析工具

# watch
watch 指令 可以 監控 指定 檔案夾下的 日誌寫入 操作

* 當新日誌寫入檔案時 自動 顯示到控制檯
* 不同 日誌以 不過顏色 格式化後 輸出
* watch 支持在運行時 輸入 指令 以 改變 watch 行爲
* 可 暫停 日誌 寫入 監控
* 允許指定 正則規則 過濾 顯示的 日誌

```bash
$ ./zap-cli watch -h
watch log configure
	zap-cli watch

Usage:
  zap-cli watch [flags]

Flags:
  -d, --directory string   logs directory (default "logs")
  -f, --filename string    log's filename (default "app.log")
  -h, --help               help for watch
  -u, --url string         zap http request address (default "http://localhost:20000")
```

**watch 運行效果 以不同 顏色 顯示日誌**
![assert/0.png](assert/0.png)

**watch 運行時 支持的指令**
![assert/3.png](assert/3.png)
**info 指令可以顯示 當前 watch狀態**
![assert/4.png](assert/4.png)